<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'type' => 'admin',
            'name' => "GG-Xicy",
            'email' => "xicy@admin.com",
            'password' => Hash::make('123123123')
        ));
        User::create(array(
            'type' => 'admin',
            'name' => "GG-Valtheim",
            'email' => "valtheim@admin.com",
            'password' => Hash::make('123123123')
        ));
        User::create(array(
            'type' => 'admin',
            'name' => "GG-Ryan",
            'email' => "ryan@admin.com",
            'password' => Hash::make('123123123')
        ));
        User::create(array(
            'type' => 'admin',
            'name' => "admin",
            'email' => "info@admin.com",
            'password' => Hash::make('1ffj7f6q')
        ));
    }
}
