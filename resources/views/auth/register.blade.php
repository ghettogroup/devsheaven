@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">

                    <div class="card-body">

                        <form id="contact-form" class="mt-5" method="post" action="{{ route('register') }}" role="form"
                              data-scroll-reveal="enter bottom move 50px over 0.6s after 0.3s"
                              style="padding-top: 100px">
                            @csrf

                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="messages text-center"></div>
                                </div>
                            </div>
                            <div class="controls">
                                <div class="row justify-content-center">
                                    <div class="col-md-8">
                                        <div class="form-group text-center">
                                            <input id="name" type="text" name="name"
                                                   class="form-control text-center"
                                                   placeholder="Enter Your Name *" required="required"
                                                   data-error="Name is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mt-3 mt-md-0">
                                        <div class="form-group text-center">
                                            <input id="password" type="Password" name="password"
                                                   class="form-control text-center"
                                                   placeholder="Enter Your Password *" required="required"
                                                   data-error="Password is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mt-3 mt-md-0">
                                        <div class="form-group text-center">
                                            <input id="password_confirmation" type="Password" name="password_confirmation"
                                                   class="form-control text-center"
                                                   placeholder="Enter Your Password Confirmation *" required="required"
                                                   data-error="Password Confirmation is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mt-3 text-center">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
