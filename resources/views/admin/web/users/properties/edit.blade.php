@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/update', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">
                            {!! $user['name'] !!}
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            Properties
                        </h3>

                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Title</label>
                                    <input type="text" class="form-control" name="extras[title]"
                                           value="{!! isset($user['extras']['title'] ) ? $user['extras']['title']  : ""!!}">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Detail </label>
                                    <textarea class="form-control" name="extras[detail]"
                                              rows="2">{!! isset($user['extras']['detail'] ) ? $user['extras']['detail'] : ""!!}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>

                            <div class="row col-sm-6 col-lg-5">
                                <div class="col-sm-6 col-lg-12">
                                    <span id="btnFile" style="cursor: pointer;">
                                        <label><b>Profile Image</b></label>
                                        <img src="{!! isset($user['extras']['profile_image'] ) ? $user['extras']['profile_image']  : "https://via.placeholder.com/100"!!}"
                                             alt="banner-photo" class="card p-1"
                                             style="width: 50%; margin-left: auto; margin-right: auto">
                                    </span>
                                    <div class="form-group" style="display: none">
                                        <div class="custom-file ">
                                            <input type="file" class="form-control" id="upload-file"
                                                   name="extras[profile_image]">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 ability">
                                    @foreach($user['properties'] as $key => $value)
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="form-label ability">{!! ucwords(deswap_prop($key)) !!}</label>
                                            </div>
                                            <div class="col-lg-6 range">
                                                <input type="range" class="form-control custom-range"
                                                       id="{{$key}}-range"
                                                       value="{{$value}}" step="5" min="0" max="100">
                                            </div>
                                            <div class="col-lg-1 number">
                                                <input type="text" class="form-control w-7 h-6"
                                                       name="properties[{{$key}}]"
                                                       id="{{$key}}-number"
                                                       value="{{$value}}">
                                            </div>
                                            <div class="col-lg-1">
                                                <span class="propDelete" data-key="{{$key}}" style="cursor: pointer;padding-left: 35px" >
                                                    <i class="fa fa-trash" style="padding-top: 7px" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                    @for($i = 1; $i<=5-count($user['properties']); $i++)
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control w-12 h-6 new_properties" id="properties_key"
                                                       placeholder="...">
                                            </div>
                                            <div class="col-lg-6 range">
                                                <input type="range" class="form-control custom-range"
                                                       value="50" step="5" min="0" max="100">
                                            </div>
                                            <div class="col-lg-1 number">
                                                <input type="text" class="form-control w-8 h-6"
                                                       name="properties[]"
                                                       value="50">
                                            </div>

                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h3 class="card-title">
                            <u>Social Media</u>
                        </h3>
                        <div class="row col-lg-8" style="padding-bottom: 10px">
                            <label class="form-label col-lg-2">Git </label>
                            <div class="col-lg-1"></div>
                            <input type="text" class="form-control col-lg-8 w-7 h-6 fa-align-left" name="extras[git]"
                                   value="{!! isset($user['extras']['git'] ) ? $user['extras']['git']  : ""!!}">
                        </div>
                        <div class="row col-lg-8" style="padding-bottom: 10px">
                            <label class="form-label col-lg-2">Linkedin </label>
                            <div class="col-lg-1"></div>
                            <input type="text" class="form-control col-lg-8 w-7 h-6 fa-align-left" name="extras[linkedin]"
                                   value="{!! isset($user['extras']['linkedin'] ) ? $user['extras']['linkedin']  : ""!!}">
                        </div>
                        <div class="row col-lg-8" style="padding-bottom: 10px">
                            <label class="form-label col-lg-2">Twitter </label>
                            <div class="col-lg-1"></div>
                            <input type="text" class="form-control col-lg-8 w-7 h-6 fa-align-left" name="extras[twitter]"
                                   value="{!! isset($user['extras']['twitter'] ) ? $user['extras']['twitter']  : ""!!}">
                        </div>
                        <div class="row col-lg-8" style="padding-bottom: 10px">
                            <label class="form-label col-lg-2">Google Plus </label>
                            <div class="col-lg-1"></div>
                            <input type="text" class="form-control col-lg-8 w-7 h-6 fa-align-left" name="extras[google-plus]"
                                   value="{!! isset($user['extras']['google-plus'] ) ? $user['extras']['google-plus']  : ""!!}">
                        </div>
                        <div class="row col-lg-8" style="padding-bottom: 10px">
                            <label class="form-label col-lg-2">Facebook </label>
                            <div class="col-lg-1"></div>
                            <input type="text" class="form-control col-lg-8 w-7 h-6 fa-align-left" name="extras[facebook]"
                                   value="{!! isset($user['extras']['facebook'] ) ? $user['extras']['facebook']  : ""!!}">
                        </div>

                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        CKEDITOR.replace('extras[detail]');

        $('#btnFile').on('click', function () {
            $('#upload-file').trigger('click');
        });

        $('.propDelete').on('click', function() {
            window.location.href = window.location.href + '/prop-delete?p='+$(this).data('key');
        });

        $(document).ready(function () {
            $('.new_properties').bind('input', function() {
                $(this).val() // get the current value of the input field.
                $(this).parent().siblings(".number").children('input').attr('name', 'properties[' + $(this).val() + ']');
            });


            $(document).delegate('[type = "range"]', 'change', function (e) {
                $(e.currentTarget).parent().siblings(".number").children('input').val(e.currentTarget.value);
            });

            $(document).delegate('[type = "number"]', 'change', function (el) {
                $(el.currentTarget).parent().siblings(".range").children('input').val(el.currentTarget.value);
            })
        });
    </script>
@endpush