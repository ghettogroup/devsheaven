@extends('admin.inc.app')

@section('content')
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this content?
                </div>
                <div class="modal-footer">
                    <a href="{!! app('request')->url()."/delete" !!}" class="btn btn-danger delete-user-href">Yes</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-11">
                                <h3 class="card-title">{!! strtoupper($type) !!}</h3>
                            </div>
                            <div class="col-lg-1">
                                <a href="/admin/users/{{$type}}/new" class="btn btn-info"
                                   style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Add New</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    @if($type == 'developer') <th>Order</th> @endif
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Mail</th>
                                </tr>
                                </thead>
                                @foreach($data as $user)
                                    <tr>
                                        @if($type == 'developer') <td> {!! $user->order !!}</td> @endif
                                        <td> {!! $user->name !!}</td>
                                        <td> {!! $user->email !!}</td>
                                        <td>
                                            <a href="/admin/users/{!! $type."/".$user->id !!}" class="btn btn-primary"
                                               style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Edit</a>
                                            @if($user->type == 'developer')
                                            <a href="/admin/users/{!! $type."/".$user->id !!}/properties"
                                               class="btn btn-success"
                                               style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Properties</a>
                                            @endif
                                            <button type="button" class="btn btn-danger delete-user" data-toggle="modal"
                                                    data-id="{!! $user->id !!}" data-target="#deleteModal"
                                                    style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).on("click", ".delete-user", function () {
            var userId = $(this).data('id');
            $(".delete-user-href").attr('href', "{!! app('request')->url()."/" !!}" + userId + "/delete");
        });
    </script>
@endpush