@extends('admin.inc.app')
@section('content')

    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url()."/create", 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">{!! strtoupper($type) !!}
                            <i class="fa fa-arrow-right"></i> New User</h3>
                    </div>
                    <div class="row col-lg-12" >
                        <div class="col-sm-6 col-lg-8" style="margin: auto">
                            <div class="custom-controls-stacked" style="text-align: center">
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="type" value="admin" {!! $type == 'admin' ? "checked" : null !!}>
                                    <span class="custom-control-label">ADMIN</span>
                                </label>
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="type" value="developer" {!! $type == 'developer' ? "checked" : null !!}>
                                    <span class="custom-control-label">DEVELOPER</span>
                                </label>
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="type" value="user" {!! $type == 'user' ? "checked" : null !!}>
                                    <span class="custom-control-label">USER</span>
                                </label>
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="type" value="coin-team" {!! $type == 'coin-team' ? "checked" : null !!}>>
                                    <span class="custom-control-label">COIN TEAM</span>
                                </label>
                            </div>
                            @if($type == 'developer')
                                <div class="form-group">
                                    <label class="form-label">ORDER</label>
                                    <input type="text" required class="form-control w-9" name="order"  autocomplete="off">
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="form-label">Name Surname</label>
                                <input type="text"  required class="form-control" name="name" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="text" class="form-control" required name="email" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" required name="password" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection