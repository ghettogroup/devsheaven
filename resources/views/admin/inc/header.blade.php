<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/">
                <img src="/img/logo-text.png" class="header-brand-img" alt="devsheaven">
            </a>
        </div>
    </div>
</div>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="/admin/users/developer" class="nav-link"><i class="fe fe-github"></i> Developers</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/users/user" class="nav-link"><i class="fe fe-user"></i> Users</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/users/admin" class="nav-link"><i class="fe fe-globe"></i> Admins</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/users/coin-team" class="nav-link"><i class="fe fe-users"></i> Coin Team</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>