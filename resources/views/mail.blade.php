<html>
<head>
    <title>New Registration</title>
</head>
<body style="margin: 0; padding: 0; width: 50%; background-color: #c5c5c5;">
<table width="100%" cellpadding="0" cellspacing="0">

    @foreach ($data as $key => $value)
        @if(is_array($value))
            {!! $value = implode(', ', $value) !!}
        @endif
        <tr>
            <th style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left; font-size:14px; border: 1px solid #f2f4f6;padding: 3px">
                {!! $key !!}
            </th>
            <td colspan="2" style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left;font-size:14px;border: 1px solid #f2f4f6; border-left:none;padding: 3px">
                {!! $value !!}
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>