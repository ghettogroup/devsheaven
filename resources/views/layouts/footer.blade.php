<div class="section padding-top-big">
    <div class="background-parallax" style="background-image: url('img/parallax-dark-3.jpg')" data-enllax-ratio=".5"
         data-enllax-type="background" data-enllax-direction="vertical">
        <div class="text-center">
            <img src="/img/logo-text.png" style="width: 50%" alt="">
        </div>
    </div>
    <div class="container padding-bottom-big">
        <div class="row justify-content-between">
            <div class="offset-lg-3 mt-4"></div>
        </div>
    </div>
    <div class="section py-4 background-dark-blue-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 footer text-center text-lg-left">
                    <p>Copyright © 2018, Devs Heaven. made by <a href="https://groupghetto.com/" target="_blank">GG Devs</a></p>
                </div>
                <div class="col-lg-6 footer mt-4 mr-auto mt-lg-0 mr-lg-0 text-center text-lg-right">
                    <a class="app-btn mx-2 mr-lg-3" href="https://twitter.com/HeavenDevs" target="_blank"><i class="fab fa-twitter"></i></a>
                    <a class="app-btn mx-2 mr-lg-3" href="https://discord.gg/K6q3j2U" target="_blank"><i class="fab fa-discord"></i></a>
                    <a class="app-btn mx-2 mr-lg-3" href="https://t.me/devsheavenchat" target="_blank"><i class="fab fa-telegram"></i></a>
                    <a class="app-btn mx-2 mr-lg-3" href="https://groupghetto.com/" target="_blank">
                        <b>Group Ghetto</b>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="scroll-to-top">to top</div>
