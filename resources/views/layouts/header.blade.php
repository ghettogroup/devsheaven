<div class="loader dark">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
			<rect x="0" y="0" width="4" height="10" fill="#333">
                <animateTransform attributeType="xml"
                                  attributeName="transform" type="translate"
                                  values="0 0; 0 20; 0 0"
                                  begin="0" dur="0.6s" repeatCount="indefinite" />
            </rect>
        <rect x="10" y="0" width="4" height="10" fill="#333">
            <animateTransform attributeType="xml"
                              attributeName="transform" type="translate"
                              values="0 0; 0 20; 0 0"
                              begin="0.2s" dur="0.6s" repeatCount="indefinite" />
        </rect>
        <rect x="20" y="0" width="4" height="10" fill="#333">
            <animateTransform attributeType="xml"
                              attributeName="transform" type="translate"
                              values="0 0; 0 20; 0 0"
                              begin="0.4s" dur="0.6s" repeatCount="indefinite" />
        </rect>
		</svg>
</div>

<div id="menu-wrap" class="menu-back cbp-af-header dark">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-light mx-lg-0">
                    <a class="navbar-brand" href="/" style="width: 40%"><img src="img/logo.png" style="width: 10%" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon">
								<span class="menu-icon__line menu-icon__line-left"></span>
								<span class="menu-icon__line"></span>
								<span class="menu-icon__line menu-icon__line-right"></span>
							</span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#stats" data-ps2id-offset="120">STATISTICS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#roadmap">Active Accounts</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#teams">Teams</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#contact">Contact</a>
                            </li>
                            <li class="nav-item mt-2">
                                @if(Auth::check())
                                    <form action="logout" method="post">
                                        @csrf
                                        <button type="submit" name="your_name" value="your_value" class="btn btn-primary js-tilt">Log Out</button>
                                    </form>
                                @else
                                    <a class="btn btn-primary js-tilt" href="#" data-toggle="modal" data-target="#login"
                                       role="button" data-tilt-perspective="300"
                                       data-tilt-speed="700" data-tilt-max="24"><span>Login</span></a>
                                    <a class="btn btn-primary js-tilt" href="#" data-toggle="modal" data-target="#register"
                                       role="button" data-tilt-perspective="300"
                                       data-tilt-speed="700" data-tilt-max="24"><span>Register</span></a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
