@extends('layouts.app')
@php
    $users = \App\User::where('type','!=', 'admin')->get()->groupBy('type');
@endphp

@section('content')
    <div class="section full-height height-auto-lg hide-over background-dark-blue-3">
        <div class="hero-center-wrap relative-on-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-center text-lg-left parallax-fade-top align-self-center z-bigger">
                        <h2 class="text-white">Welcome to DevsHeaven</h2>
                        <p class="mt-3 mb-4 pb-3 font-weight-normal text-white">
                            The first blockchain developer platform. We have renewed our website to better serve you.
                        </p>
                    </div>
                    <div class="col-lg-6 mt-5 mt-lg-0">
                        <div class="img-wrap header-img">
                            <img src="img/slider.png" style="width: 100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ocean dark">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
        <div id="particles-js" class="min-height"></div>
    </div>

    <div class="section z-bigger background-gradient-dark">
        <div class="section padding-bottom-big" id="stats">
            <div class="section padding-top-big">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-wrap text-center">
                                <div class="back-title">Statistics</div>
                            </div>
                        </div>
                        @foreach($users as $key => $user)
                            <div class="col-xl-4 col-sm-6 mt-4 mt-xl-0"
                                 data-scroll-reveal="enter bottom move 50px over 0.6s after 0.7s">
                                <div class="table-sale background-dark-blue-3 future-price">
                                    <p class="text-center"><span>{!! strtoupper($key) !!}S</span></p>
                                    <div class="table-line mt-4 mb-4"></div>
                                    <h4 class="text-center">{!! $user->count() !!}</h4>
                                    <div class="table-line mt-4 mb-4"></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section padding-top-bottom-big" id="roadmap">
        <div class="background-parallax" style="background-image: url('img/parallax-dark.jpg')" data-enllax-ratio=".5"
             data-enllax-type="background" data-enllax-direction="vertical"></div>
        <div class="container">
            <div class="text-center">
                <a class="btn btn-primary btn-lg js-tilt" href="https://jobs.devsheaven.com/" target="_blank">
                    <span>SENT DISCORD REQUEST FOR A CRYPTO EXPERT</span>
                </a>
            </div>
            {{--<div class="row">
                <div class="col-md-12">
                    <div class="title-wrap text-center">
                        <div class="back-title">roadmap</div>
                        <h3 class="text-white">Making the most<br>out of unused.</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="customNavigation">
                        <a class="prev-roadmap dark"><i class="fas fa-angle-left"></i></a>
                        <a class="next-roadmap dark"><i class="fas fa-angle-right"></i></a>
                    </div>
                    <div id="owl-roadmap" class="owl-carousel owl-theme">
                        <div class="item dark">
                            <div class="roadmap-box background-dark-blue-2">
                                <p class="mb-4 achieved"><span>Achieved</span></p>
                                <h6 class="text-white">Dec 2017.</h6>
                                <p class="text-grey">Start of Development</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box current-road background-dark-blue-2 roadmap-shadow">
                                <p class="mb-4"><span>Current</span></p>
                                <h6 class="text-white">Apr 2018.</h6>
                                <p class="text-grey">Token Sale Start</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box next-road background-dark-blue-2">
                                <p class="mb-4 plan"><span>Plan</span></p>
                                <h6 class="text-white">May 2018.</h6>
                                <p class="text-grey">Blockchain Dev Starts</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box next-road background-dark-blue-2">
                                <p class="mb-4 plan"><span>Plan</span></p>
                                <h6 class="text-white">Oct 2018.</h6>
                                <p class="text-grey">Exchange Beta Launch</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box next-road background-dark-blue-2">
                                <p class="mb-4 plan"><span>Plan</span></p>
                                <h6 class="text-white">Jan 2019.</h6>
                                <p class="text-grey">Expanded Licensing</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box next-road background-dark-blue-2">
                                <p class="mb-4 plan"><span>Plan</span></p>
                                <h6 class="text-white">Mar 2019.</h6>
                                <p class="text-grey">Debit Card Launch</p>
                            </div>
                        </div>
                        <div class="item dark">
                            <div class="roadmap-box next-road background-dark-blue-2">
                                <p class="mb-4 plan"><span>Plan</span></p>
                                <h6 class="text-white">May 2019.</h6>
                                <p class="text-grey">Partnership For Future</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>

    </div>

    <div class="section padding-top-bottom-big" id="teams">
        <div class="background-parallax" style="background-image: url('img/parallax-dark-1.jpg')" data-enllax-ratio=".5"
             data-enllax-type="background" data-enllax-direction="vertical"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-wrap text-center">
                        <div class="back-title">TOP</div>
                        <h3 class="text-white">Developers</h3>
                        <!--<p class="color-pr">We're a close team of creatives, designers & developers who work together.</p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                @foreach($developers as $developer)
                    @php($socials = array_intersect_key($developer['extras'], array_flip($social)))
                    @if($loop->iteration == 3 || $loop->iteration == 8)
                        <div class="col-lg-4 col-md-6" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.7s">
                            @else
                                <div class="col-lg-4 col-md-6 mb-5 mb-lg-0"
                                     data-scroll-reveal="enter bottom move 50px over 0.6s after 0.3s">
                                    @endif
                                    <div class="team-wrap dark">
                                        <a class="d-inline" data-toggle="modal"
                                           data-target="#{!! getArrayData($developer, 'id') !!}">
                                            <div class="team-img-wrap dark js-tilt" data-tilt-perspective="700"
                                                 data-tilt-speed="700"
                                                 data-tilt-max="24">
                                                <img src="{!! getArrayData($developer['extras'], 'profile_image') !!}"
                                                     alt="" width="150px" height="225px">
                                                <div class="team-img-mask"></div>
                                            </div>
                                        </a>
                                        @if($loop->iteration == 1)
                                            <h6 class="fas fa-crown" style="color:gold"></h6>
                                        @elseif($loop->iteration == 2)
                                            <h6 class="fas fa-crown" style="color:silver"></h6>
                                        @elseif($loop->iteration == 3)
                                            <h6 class="fas fa-crown" style="color:#cd7f32"></h6>
                                        @endif
                                        <p>{!! getArrayData($developer, 'name') !!}</p>
                                        <div style="padding-bottom: 5px">
                                            @foreach($socials as $k => $v)
                                                @if(is_null($v)) @continue @endif
                                                <a class="app-btn ml-3" href="{{$v}}" target="_blank"><i
                                                            class="fab fa-{{$k}}"></i></a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @if($loop->iteration == 3 )
                                    <div class="col-md-12 padding-top-small"></div>
                                @endif
                                @endforeach
                        </div>
            </div>
        </div>
    </div>

    <!-- Modal Team 1-->
    @foreach($developers as $developer)
        <div class="modal fade bd-example-modal-lg" id="{!! getArrayData($developer, 'id') !!}" tabindex="-1"
             role="dialog" aria-labelledby="{!! getArrayData($developer, 'id') !!}"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content dark">
                    <div class="modal-img-wrap"
                         style="background-image: url('{!! getArrayData($developer['extras'], 'profile_image') !!}')"></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    <h3 class="mb-0">{!! getArrayData($developer, 'name') !!}</h3>
                                    <p class="lead">{!! getArrayData($developer['extras'], 'title') !!}</p>
                                </div>
                                <div class="col-xl-12">
                                    {!! getArrayData($developer['extras'], 'detail') !!}

                                    @foreach($developer['properties'] as $k=>$v)
                                        <p class="mt-4 mb-0">
                                            <small>{!! deswap_prop($k) !!}</small>
                                            @if(Auth::check())
                                                <span class="vote float-right" data-prop="{{enswap_prop($k)}}">
                                                    <i class="far fa-thumbs-up"></i>
                                                    <i class="far fa-thumbs-down"></i>
                                                </span>
                                            @endif
                                        </p>
                                        <div class="progress mt-1 mb-2">
                                            <div class="progress-bar" style="width: {!! $v !!}%" role="progressbar"
                                                 aria-valuenow="{!! $v !!}"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    @endforeach
                                </div>
                                {{--Post Form--}}
                                @if(Auth::check())
                                    <div class="col-md-12 mt-2 mb-2">
                                        <form method="post" action="{{route("propertiesSend")}}" role="form">
                                            @csrf
                                            <input type="hidden" name="user" value="{{$developer['id']}}">
                                            @foreach($developer['properties'] as $k=>$v)
                                                <input type="hidden" name="{{$k}}"/>
                                            @endforeach
                                            <div class="row justify-content-center">
                                                <div class="col-md-12">
                                                    <div class="form-group text-center">
											<textarea id="form_message" name="message" class="form-control text-center"
                                                      placeholder="Your message *" rows="2" required="required"
                                                      data-error="Please,leave us a message."></textarea>
                                                        <div class="help-block with-errors textarea-error"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 text-center">
                                                    <input type="submit" class="btn btn-primary btn-send text-center"
                                                           value="Send message">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <!-- Modal Register-->
    <div class="modal fade bd-example-modal-lg" id="register" tabindex="-1" role="dialog" aria-labelledby="register"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content dark">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <form id="register-form" method="post" action="{{ route('register') }}" role="form"
                                      data-scroll-reveal="enter bottom move 50px over 0.6s after 0.3s">
                                    @csrf

                                    <div class="row justify-content-center">
                                        <div class="col-md-8">
                                            <div class="messages text-center">
                                                @foreach ($errors->all() as $message)
                                                    <p>{!! $message !!}</p>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="form_name" value="register">
                                    <div class="controls">
                                        <div class="row justify-content-center">
                                            <div class="col-md-8">
                                                <div class="form-group text-center">
                                                    <input id="name" type="text" name="name"
                                                           class="form-control text-center"
                                                           placeholder="Enter Your Name *" required="required"
                                                           data-error="Name is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-3 mt-md-0">
                                                <div class="form-group text-center">
                                                    <input id="email" type="text" name="email"
                                                           class="form-control text-center"
                                                           placeholder="Enter Your Email *" required="required"
                                                           data-error="Email is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-3 mt-md-0">
                                                <div class="form-group text-center">
                                                    <input id="password" type="Password" name="password"
                                                           class="form-control text-center"
                                                           placeholder="Enter Your Password *" required="required"
                                                           data-error="Password is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-3 mt-md-0">
                                                <div class="form-group text-center">
                                                    <input id="password_confirmation" type="Password"
                                                           name="password_confirmation"
                                                           class="form-control text-center"
                                                           placeholder="Enter Your Password Confirmation *"
                                                           required="required"
                                                           data-error="Password Confirmation is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-3 text-center">
                                                <input type="submit" class="btn btn-send btn-primary"
                                                       value="Submit">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Login-->
    <div class="modal fade bd-example-modal-lg" id="login" tabindex="-1" role="dialog" aria-labelledby="login"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content dark">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <form id="login-form" method="post" action="{{ route('login') }}" role="form">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="col-md-8">
                                            <div class="messages text-center">
                                                @foreach ($errors->all() as $message)
                                                    <p>{!! $message !!}</p>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="form_name" value="login">
                                    <div class="controls">
                                        <div class="row justify-content-center">
                                            <div class="col-md-8">
                                                <div class="form-group text-center">
                                                    <input id="login_email" type="email" name="email"
                                                           class="form-control text-center"
                                                           placeholder="Enter Email *" required="required"
                                                           data-error="Email is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-3 mt-md-0">
                                                <div class="form-group text-center">
                                                    <input id="login_password" type="password" name="password"
                                                           class="form-control text-center"
                                                           placeholder="Enter your password *" required="required"
                                                           data-error="Password is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-md-8 mt-3 mb-3 text-center">
                                                <input type="submit" class="btn btn-primary btn-send text-center"
                                                       value="Login">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section padding-top-bottom-big" id="contact">
        <div class="background-parallax" style="background-image: url('img/parallax-dark-2.jpg')"
             data-enllax-ratio=".5"
             data-enllax-type="background" data-enllax-direction="vertical"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-wrap text-center">
                        <div class="back-title">contact</div>
                        <h3 class="text-white">Always ready to work</h3>
                        <p class="color-pr">Get in touch.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.3s">
                    <div class="contact-det text-center">
                        <a href="https://discord.gg/K6q3j2U" target="_blank" style="color: white; text-decoration: none;">
                            <i class="fab fa-discord fa-6x"></i>
                            <h6 class="text-white">Discord</h6>
                            Join Us
                        </a>
                    </div>
                </div>
                <div class="col-md-3 mt-4 mt-md-0" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.5s">
                    <div class="contact-det text-center">
                        <a href="mailto:devsheavencontact@gmail.com" style="color: white; text-decoration: none;">
                            <i class="far fa-envelope fa-6x"></i>
                            <h6 class="text-white">Email</h6>
                            devsheavencontact@gmail.com
                        </a>
                    </div>
                </div>
                <div class="col-md-3 mt-4 mt-md-0" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.7s">
                    <div class="contact-det text-center">
                        <a href="https://t.me/devsheavenchat" target="_blank" style="color: white; text-decoration: none;">
                            <i class="fab fa-telegram fa-6x" style="color: white;"></i>
                            <h6 class="text-white">Telegram</h6>
                            Join US
                        </a>
                    </div>
                </div>
            </div>

            <form id="contact-form2" method="post" action="{{ app('request')->url() }}" role="form" style="padding-top: 10px"
                  data-scroll-reveal="enter bottom move 50px over 0.6s after 0.3s">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="alert alert-danger msg-error"
                             style="display: none">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="controls">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <div class="form-group text-center">
                                <input id="form_name" type="text" name="name" class="form-control text-center"
                                       placeholder="Enter your first name *" required="required"
                                       data-error="First name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-4 mt-3 mt-md-0">
                            <div class="form-group text-center">
                                <input id="form_email" type="email" name="email" class="form-control text-center"
                                       placeholder="Enter your email *" required="required"
                                       data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 mt-3">
                            <div class="form-group text-center">
                                <select class="form-control text-center selectpicker" id="form_type" multiple data-live-search="true" size name="properties[]" style="color: #7b5eea">
                                    <option>New Crypto Coin Development</option>
                                    <option>Crypto Bugfixing/Add-Ons</option>
                                    <option>Ethereum - Contracts</option>
                                    <option>Wallet Designer</option>
                                    <option>Mining Tools/Experts</option>
                                    <option>Mobile Wallets</option>
                                    <option>Coin Logos/Graphics</option>
                                    <option>Whitepaper/Proof-Reading</option>
                                    <option>Crypto/ICO Websites</option>
                                    <option>Video Animation</option>
                                    <option>Discord/Telegram Marketing</option>
                                    <option>YouTube Marketing</option>
                                    <option>Website Frontend</option>
                                    <option>Website Backend</option>
                                    <option>Other</option>
                                </select>

                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 mt-3">
                            <div class="form-group text-center">
                            <textarea id="form_message" name="message" class="form-control text-center"
                                      placeholder="Your message *" rows="4" required="required"
                                      data-error="Please,leave us a message."></textarea>
                                <div class="help-block with-errors textarea-error"></div>
                            </div>
                        </div>
                        <div class="col-md-8 mt-2 text-center d-inline-block">
                            <div class="g-recaptcha" id="recaptcha" data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"></div>
                        </div>
                        <div class="col-md-8 mt-2 text-center">
                            <button type="submit" class="btn btn-primary js-tilt btn-submit">Send message</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        $( '.btn-submit' ).click(function(e){
            e.preventDefault();
            var $captcha = $( '#recaptcha' ),
                response = grecaptcha.getResponse();

            if (response.length === 0) {
                $( '.msg-error > ul').html("<li>reCAPTCHA is mandatory </li>" ).closest("div").show();
                if( !$captcha.hasClass( "error" ) ){
                    $captcha.addClass( "error" );
                }
            } else {
                $( '.msg-error > ul' ).html('').closest("div").hide();
                $captcha.removeClass( "error" );
                $("#contact-form2").submit();
            }
        });


        $('.selectpicker').selectpicker("setStyle", "form-control");
        @if(session()->get('form_name'))
        $("#{{session()->get('form_name')}}").modal();
        {{session()->forget('form_name')}}
        @endif
    </script>
@endpush
