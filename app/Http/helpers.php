<?php
/**
 * Created by PhpStorm.
 * User: alper
 * Date: 22.08.2018
 * Time: 19:51
 */
function getArrayData($array, $key, $default = null)
{
    if (array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        if ($default != null) {
            return $default;
        } else {
            return null;
        }
    }
}

function enswap_prop($str)
{
    $hold = ['++' => '-plusplus', '#' => '-sharp', '/' => '-slash', '+' => "-plus"];
    $data = $str;
    foreach ($hold as $key => $value) {
        if (strstr($data, $key) != false) {
            $data = str_replace($key, $value, $data);
        }
    }
    return $data;
}

function deswap_prop($str)
{
    $hold = ['++' => '-plusplus', '#' => '-sharp', '/' => '-slash', '+' => "-plus"];
    $data = $str;
    foreach ($hold as $key => $value) {
        if(strstr($data, $value) != false) {
            $data = str_replace($value, $key, $data);
        }
    }
    return $data;
}

function getCurrentUser()
{
    $user = \Illuminate\Support\Facades\Auth::user();
    $hold = $user->toArray();
    unset($hold['created_at'], $hold['updated_at'], $hold['deleted_at']);

    $data = $hold;
    foreach ($user->extras as $u) {
        $data['extras'][$u->key] = $u->value;
    }
    foreach ($user->properties as $u) {
        $data['properties'][$u->key] = $u->value;
    }
    return $data;
}

function getUser($id)
{
    $user = \App\User::find($id);
    $hold = $user->toArray();
    unset($hold['created_at'], $hold['updated_at'], $hold['deleted_at']);

    $data = $hold;
    $data['extras'] = [];
    foreach ($user->extras as $u) {
        $data['extras'][$u->key] = $u->value;
    }

    $data['properties'] = [];
    foreach ($user->properties as $u) {
        $data['properties'][$u->key] = $u->value;
    }
    return $data;
}

//Tarih Gün-Ay(String)-Yıl
function strtodate($date, $day = 1, $month = 1, $year = 1)
{
    $date = \Carbon\Carbon::parse($date);
    if ($day == 1) {
        $ret[] = 'j';
    }
    if ($month == 1) {
        $ret[] = "F";
    }
    if ($year == 1) {
        $ret[] = "Y";
    }
    $format = @implode(' ', $ret);

    return date($format, strtotime($date));
}

function slugify($text)
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}