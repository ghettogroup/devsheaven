<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;

class AdminController extends Controller
{

    public function index()
    {
        if(Auth::user()->type == "admin") {
            return view('admin.web.users');
        } else {
            return Redirect::route('home');
        }
    }

    public function doLogin()
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'type'  => "admin"
        );
        if (Auth::attempt($userdata)) {
            return Redirect::to('/admin/home');
        } else {
            return Redirect::to('/admin')->withErrors('loginError', 'Nereye giriyorsunuz acabaa?');
        }
    }
}

?>