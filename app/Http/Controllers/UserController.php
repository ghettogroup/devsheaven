<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Admin\PageDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($type)
  {
      if(in_array($type, ['admin', 'developer', 'user', 'coin-team'])) {
          $data = User::where('type', $type)->orderBy('order')->get();
          return view('admin.web.users.main', compact('data', 'type'));
      } else {
          return redirect()->back();
      }
  }

  public function new($type)
  {
      if(in_array($type, ['admin', 'developer', 'user', 'coin-team'])) {
          return view('admin.web.users.new', compact( 'type'));
      } else {
          return redirect()->to('/admin/users/'.$type);
      }
  }


    public function create($type, Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if($user) {
            return redirect()->route('user.main', $data['type']);
        }
    }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($type, $id = null)
  {
      $user = User::where('type', $type)->find($id);
      if($user) {
          return view('admin.web.users.edit', compact('user', 'type'));
      }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($type, $id, Request $request)
  {
      $user = User::find($id);
      if($user) {
          $data = $request->all();
          unset($data['_token']);
          if(Hash::check($data['password'], $user->password)) {
              unset($data['password']);
          } else {
              $data['password'] = Hash::make($data['password']);
          }
          $user->update($data);
          $user->save();
      }
      return redirect()->to('/admin/users/'.$type);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function delete($type, $id)
  {
      $user = User::find($id);
      if($user && Auth::user() != $user) {
          $user->delete();
      }
      return redirect()->to('/admin/users/'.$type);
  }
  
}

?>