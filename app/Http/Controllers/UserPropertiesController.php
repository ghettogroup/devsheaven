<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Admin\PageDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserPropertiesController extends Controller
{

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($type, $id = null)
  {
      $user = getUser($id);

      if($user) {
          return view('admin.web.users.properties.edit', compact('user', 'type'));
      }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($type, $id, Request $request)
  {
      $data = $request->all();
      $user = User::find($id);
      if($user) {
          unset($data['_token']);
          if(isset($data['extras'])) {
            foreach ($data['extras'] as $k => $v) {
                $file = isset($request->file('extras')[$k]);
                if ($file) {
                    $file = $request->file('extras')[$k];
                    $fileName = $user->id."-".$file->getClientOriginalName();
                    $file->storeAs('', $fileName);
                    $user->extras()->updateOrCreate(['key' => $k], ['value' => Storage::disk('public')->url($fileName)]);
                }else {
                    $user->extras()->updateOrCreate(['key' => $k], ['value' => $v]);
                }
            }
          }
          if(isset($data['properties'])) {
            foreach ($data['properties'] as $k => $v) {
                if(is_int($k)) continue;
                $user->properties()->updateOrCreate(['key' => enswap_prop($k)], ['value' => $v]);
            }
          }
      }
      return redirect()->back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function delete($type, $id)
  {
      $user = User::find($id);
      if($user && Auth::user() != $user) {
          $user->delete();
      }
      return redirect()->back();
  }

    public function propDelete($type, $id, Request $request)
    {

        $data = $request->all();
        $user = User::find($id);
        if($user && isset($data['p'])) {
            $user->properties()->where('key', $data['p'])->delete();
        }
        return redirect()->back();
    }
}

?>