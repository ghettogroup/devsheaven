<?php

namespace App\Http\Controllers;

use App\User;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->method() == 'post') {
            return $this->formSend($request);
        }

        $users = \App\User::where('type', 'developer')->orderBy('order')->get()->take(8);
        $social = ['git', 'facebook', 'twitter', 'google-plus', 'linkedin'];

        $developers = [];
        foreach ($users as $user) {
            $hold = $user->toArray();
            unset($hold['created_at'], $hold['updated_at'], $hold['deleted_at']);
            $developers[$user->id] = $hold;
            $developers[$user->id]['extras'] = [];
            foreach ($user->extras as $u) {
                $developers[$user->id]['extras'][$u->key] = $u->value;
            }
            $developers[$user->id]['properties'] = [];
            foreach ($user->properties as $u) {
                $developers[$user->id]['properties'][$u->key] = $u->value;
            }
        }
        return view('home', compact('developers', 'social'));
    }

    public function formSend(Request $request)
    {
        $data = $request->all();
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        unset($data['_token']);
        $html = '
         <html>
            <head>
              <title>New Registration</title>
            </head>
            <body style="margin: 0; padding: 0; width: 50%; background-color: #c5c5c5;">
              <table width="100%" cellpadding="0" cellspacing="0">';

        foreach ($data as $key => $value) {
            if(is_array($value)) {
                $value = implode(', ', $value);
            }
            $html .= '<tr><th style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left; font-size:14px; border: 1px solid #f2f4f6;padding: 3px">
                            '.$key.'
                        </th>
                    <td colspan="2" style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left;font-size:14px;border: 1px solid #f2f4f6; border-left:none;padding: 3px">
                            '.$value.'
                    </td></tr>';
        }
        $html .='
              </table>
            </body>
            </html>
        ';
        $mailer = new PHPMailer(true);

        try {
            $mailer->isSMTP();
            $mailer->Host = 'smtp.gmail.com';
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = 'ssl';
            $mailer->Username = 'devsheavencontact@gmail.com';
            $mailer->Password = 'gg99!!88';
            $mailer->Port = 465;

            $mailer->setFrom('devsheavencontact@gmail.com', 'Devsheaven');
            $mailer->addAddress('devsheavencontact@gmail.com', 'Devsheaven');

            $mailer->isHTML(true);
            $mailer->Subject = 'New Registration';
            $mailer->Body = $html;
            $mailer->send();
            $mailer->ClearAllRecipients();
            return redirect()->route('home');

        } catch (Exception $e) {
            echo "EMAIL SENDING FAILED. INFO: " . $mailer->ErrorInfo;
            return redirect()->route('home');
        }

    }

    public function propertiesSend(Request $request) {
        $data = $request->all();
        $user = User::find($data['user']);
        $message = $data['message'];
        unset($data['_token'], $data['user'], $data['message']);
        $properties = $data;
        $html = '
         <html>
            <head>
              <title>Developer Vote</title>
            </head>
            <body style="margin: 0; padding: 0; width: 50%; background-color: #c5c5c5;">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <th style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left; font-size:14px; border: 1px solid #f2f4f6;padding: 3px">
                        Developer Name
                    </th>
                    <td colspan="2" style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left;font-size:14px;border: 1px solid #f2f4f6; border-left:none;padding: 3px">
                        '.$user->name.'
                    </td></tr>';

                    foreach ($properties as $key => $prop) {
                        $html .= '<tr><th style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left; font-size:14px; border: 1px solid #f2f4f6;padding: 3px">
                            '.$key.'
                        </th>
                    <td colspan="2" style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left;font-size:14px;border: 1px solid #f2f4f6; border-left:none;padding: 3px">
                            '.$prop.'
                    </td></tr>';
                    }
                $html .='<tr><th style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left; font-size:14px; border: 1px solid #f2f4f6;padding: 3px">
                        Message
                    </th>
                    <td colspan="2" style="font-family: Arial, Helvetica Neue, Helvetica sans-serif; text-align:left;font-size:14px;border: 1px solid #f2f4f6; border-left:none;padding: 3px">
                        '.$message.'
                    </td>
                </tr>
              </table>
            </body>
            </html>
        ';

        $mailer = new PHPMailer(true);
          try {
            $mailer->isSMTP();
            $mailer->Host = 'smtp.gmail.com';
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = 'ssl';
            $mailer->Username = 'devsheavencontact@gmail.com';
            $mailer->Password = 'gg99!!88';
            $mailer->Port = 465;

            $mailer->setFrom('devsheavencontact@gmail.com', 'Devsheaven');
            $mailer->addAddress('devsheavencontact@gmail.com', 'Devsheaven');

            $mailer->isHTML(true);
            $mailer->Subject = 'Developer Vote';
            $mailer->Body = $html;
            $mailer->send();
            $mailer->ClearAllRecipients();
            return redirect()->route('home');

        } catch (Exception $e) {
            echo "EMAIL SENDING FAILED. INFO: " . $mailer->ErrorInfo;
            return redirect()->route('home');
        }
    }
}
