<?php

namespace App\Http\Controllers;

use App\UserExtra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile');
    }


    public function saveInfo(Request $request)
    {

        if(! Auth::check()) {
            return Redirect::back(404);
        }
        $user = Auth::user()->with('extras')->first();
        $data = $request->all();

        unset($data['_token']);

        foreach ($data as $key => $value) {
            if($value != null) {
                $file = $request->file($key);
                if ($file != null) {
                    $fileName = $user->id."-".$file->getClientOriginalName();
                    $file->storeAs('', $fileName);
                    UserExtra::updateOrCreate(['key' => $key], ['value' => Storage::disk('public')->url($fileName), 'user_id' => $user->id]);
                }else {
                    UserExtra::updateOrCreate(['key' => $key], ['value' => $value, 'user_id' => $user->id]);
                }
            }
        }
        return view('profile');
    }
}
