<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProperties extends Model
{
    protected $table = 'user_properties';
    public $timestamps = false;
    protected $fillable = array('user_id', 'key', 'value');
    protected $visible = array('user_id', 'key', 'value');

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
