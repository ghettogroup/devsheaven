<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/', 'HomeController@formSend')->name('formSend');
Route::post('/properties-send', 'HomeController@propertiesSend')->name('propertiesSend');

Route::get('/admin', function () {
    if(Auth::check() && Auth::user()->type == "admin") {
        return redirect()->to('/admin/users/developer');
    } else {
        return Redirect::route('home');
    }
});

Route::middleware(['auth'])->prefix('/admin')->group(function () {

    Route::get('/home', function () {
        return view('admin.web.home');
    });

    Route::prefix('/users/{type}')->group(function () {
        Route::get('/', 'UserController@index')->name('user.main');
        Route::get('/new', 'UserController@new');
        Route::post('/new/create', 'UserController@create');
        Route::get('/{id}', 'UserController@edit');
        Route::post('/{id}/update', 'UserController@update');
        Route::get('/{id}/delete', 'UserController@delete');


        Route::prefix('/{id}/properties')->group(function () {
            Route::get('/', 'UserPropertiesController@edit');
            Route::post('/update', 'UserPropertiesController@update');

            Route::get('/prop-delete', 'UserPropertiesController@propDelete');
        });
    });
});

